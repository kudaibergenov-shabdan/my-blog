import React from 'react';
import {NavLink} from "react-router-dom";
import './Header.css';

const Header = () => {
    return (
        <div className="Header">
            <header className="header">
                <h1 className="header__title">My Blog</h1>
                <nav className="header__main-nav">
                    <ul className="header__list">
                        <li className="header__list-item">
                            <NavLink className="header__list-item-link" exact to="/">Home</NavLink>
                        </li>
                        <li className="header__list-item">
                            <NavLink className="header__list-item-link" to="/posts/add">Add</NavLink>
                        </li>
                        <li className="header__list-item">
                            <NavLink className="header__list-item-link" to="/about">About</NavLink>
                        </li>
                        <li className="header__list-item">
                            <NavLink className="header__list-item-link" to="/contacts">Contacts</NavLink>
                        </li>
                    </ul>
                </nav>
            </header>
        </div>
    );
};

export default Header;