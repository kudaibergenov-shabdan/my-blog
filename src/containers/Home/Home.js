import React, {useEffect, useState} from 'react';
import axiosApi from "../../axiosApi";
import './Home.css';
import Post from "../../components/Post/Post";

const Home = ({history}) => {

    const [posts, setPosts] = useState({});

    useEffect(() => {
        const fetchData = async () => {
            const response = await axiosApi.get('/posts.json');
            return response.data;
        };

        fetchData()
            .then((resolve) => setPosts(resolve))
            .catch(error => console.log(error));
    }, []);

    const readMoreHandler = postId => {
        history.replace('/posts/' + postId);
    }

    return (
        <>
            <div className="Home">
                {Object.keys(posts).map(postId => (
                    <Post
                        key={postId}
                        postId={posts[postId]}
                        onClickPost={() => readMoreHandler(postId)}
                    />
                ))}
            </div>
        </>
    );
};

export default Home;