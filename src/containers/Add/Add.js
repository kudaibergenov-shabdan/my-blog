import React, {useState} from 'react';
import axiosApi from "../../axiosApi";
import dayjs from "dayjs";
import Editor from "../../components/UI/Editor/Editor";

const Add = ({history}) => {
    const [post, setPost] = useState(null);

    const onInputChange = e => {
        const {name, value} = e.target;
        setPost(prev => {
           return {
               ...prev,
               [name]: value,
               createdDate: dayjs().format('DD.MM.YYYY HH:mm:ss')
           }
        });
    };

    const savePost = async e => {
        e.preventDefault();
        try {
            await axiosApi.post('/posts.json', {
                post
            });
        } finally {
            history.push('/');
        }
    };

    return (
        <Editor
            pageTitle="Add new post"
            onInputChange={e => onInputChange(e)}
            submitForm={e => savePost(e)}
            buttonName="Add"
        />
    );
};

export default Add;