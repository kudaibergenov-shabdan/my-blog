import React, {useEffect, useState} from 'react';
import axiosApi from "../../axiosApi";

const PostContent = ({history, match}) => {
    const [post, setPost] = useState(null);

    useEffect(() => {
        const fetchData = async () => {
            const response = await axiosApi.get('/posts/' + match.params.id + '.json');
            return response.data;
        };
        fetchData()
            .then(resolve => setPost(resolve['post']))
            .catch(error => console.log(error));
    }, [match.params.id]);

    const updatePost = () => {
        history.replace(`/posts/${match.params.id}/edit`);
    };

    const removePost = () => {
        const deleteData = async () => {
            await axiosApi.delete('posts/' + match.params.id + '.json');
        };
        deleteData()
            .then(() => history.push("/"))
            .catch(error => console.log(error));
    };

    return (
        <>
            {post && (
                <div>
                    <p>Created Date: {post.createdDate}</p>
                    <p>Title: {post.title}</p>
                    <p>Description: {post.description}</p>
                    <button onClick={updatePost}>Update</button>
                    <button onClick={removePost}>Remove</button>
                </div>
            )}
        </>
    );
};

export default PostContent;