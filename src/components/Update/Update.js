import React, {useEffect, useState} from 'react';
import axiosApi from "../../axiosApi";
import Editor from "../UI/Editor/Editor";

const Update = ({history, match}) => {
    const [post, setPost] = useState(null);

    useEffect(() => {
        const fetchData = async () => {
            const response = await axiosApi.get('/posts/' + match.params.id + '.json');
            return response.data;
        }
        fetchData()
            .then(resolve => setPost(resolve['post']))
            .catch(error => console.log(error));
    }, [match.params.id]);

    const onInputChange = e => {
        const {name, value} = e.target;
        setPost(prev => {
            return {
                ...prev,
                [name]: value
            }
        });
    };

    const updatePost = async e => {
        e.preventDefault();
        try {
            await axiosApi.put('/posts/' + match.params.id + '.json', {
                post
            });
        } finally {
            history.push('/');
        }
    };

    return (
        <Editor
            pageTitle="Update post"
            post={post}
            onInputChange={e => onInputChange(e)}
            submitForm={e => updatePost(e)}
            buttonName="Update"
        />
    );
};

export default Update;