import React from 'react';
import './Post.css';

const Post = ({postId, onClickPost}) => {
    return (
        <div className="post-box">
            <p>Created on: {postId.post.createdDate}</p>
            <p>{postId.post.title}</p>
            <button onClick={onClickPost}>Read more >></button>
        </div>
    );
};

export default Post;