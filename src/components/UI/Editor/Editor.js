import React from 'react';

const Editor = props => {
    const post = (props.post) ? props.post : "";

    return (
        <form onSubmit={props.submitForm}>
            <h3>{props.pageTitle}</h3>
            <div>
                <p>Title</p>
                <div>
                    <input
                        name="title"
                        type="text"
                        defaultValue={post.title}
                        onChange={props.onInputChange}
                    />
                </div>
            </div>
            <div>
                <p>Description</p>
                <div>
                    <textarea
                        name="description"
                        defaultValue={post.description}
                        onChange={props.onInputChange}
                    />
                </div>
            </div>
            <div>
                <button>{props.buttonName}</button>
            </div>
        </form>
    );
};

export default Editor;