import React from "react";
import {BrowserRouter, Route, Switch} from "react-router-dom";
import './App.css';
import Home from "./containers/Home/Home";
import Header from "./containers/MainPage/Header";
import Add from "./containers/Add/Add";
import About from "./containers/About/About";
import Contacts from "./containers/Contacts/Contacts";
import PostContent from "./components/PostContent/PostContent";
import Update from "./components/Update/Update";

const App = () => (
    <div className="App">
        <BrowserRouter>
            <Header/>
            <div className="content-block">
                <Switch>
                    <Route path="/" exact component={Home}/>
                    <Route path="/posts" exact component={Home}/>
                    <Route path="/posts/add" component={Add}/>
                    <Route path='/posts/:id/edit' component={Update}/>
                    <Route path='/posts/:id' component={PostContent}/>
                    <Route path="/about" component={About}/>
                    <Route path="/contacts" component={Contacts}/>
                    <Route render={() => <h1>Not found</h1>}/>
                </Switch>
            </div>
        </BrowserRouter>
    </div>
);

export default App;
